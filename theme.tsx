import { extendTheme } from "@chakra-ui/react";

const theme = extendTheme({
  fonts: {
    inter: `'Inter', sans-serif`,
    dmmono: `'DM Mono', monospace`,
  },
});

export default theme;
