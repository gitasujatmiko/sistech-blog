import { Box, HStack, UnorderedList, ListItem, Text } from "@chakra-ui/layout";

const Achievement = () => {
  return (
    <Box px="5" py="200" bg="#4dcca3" w="100%">
      <HStack spacing="100px" display="flex" justifyContent="center">
        <Text fontSize="4xl" fontWeight="700">
          Achievements
        </Text>
        <UnorderedList spacing="3">
          <ListItem>
            <Text fontSize="lg" fontWeight="600">
              <a href="https://www.coursera.org/account/accomplishments/specialization/DC86GMFAM22M">
                Architecting with Google Compute Engine, Specialization
                Certificate
              </a>
            </Text>
            <Text fontSize="md" fontWeight="400" fontFamily="dmmono">
              Google Cloud, Coursera (Earned on April 20, 2022)
            </Text>
          </ListItem>
          <ListItem>
            <Text fontSize="lg" fontWeight="600">
              <a href="https://www.cloudskillsboost.google/public_profiles/ac4540c0-c1c7-42cf-acb4-0cffeee6efc2">
                Google Cloud Skills Badges
              </a>
            </Text>
            <Text fontSize="md" fontWeight="400" fontFamily="dmmono">
              Google Cloud Skills Boost (03/2022 - 05/2022)
            </Text>
          </ListItem>
          <ListItem>
            <Text fontSize="lg" fontWeight="600">
              1st Place DAQ (Desain Aplikasi Quran) MTQ UI
            </Text>
            <Text fontSize="md" fontWeight="400" fontFamily="dmmono">
              University of Indonesia, 2021
            </Text>
          </ListItem>
        </UnorderedList>
      </HStack>
    </Box>
  );
};

export default Achievement;
