import { Box, Center, Heading, Text, VStack } from "@chakra-ui/layout";

const Hero = () => {
  return (
    <Box px="5" pt="200" pb="250" w="100%">
      <VStack spacing="8" fontFamily="inter">
        <Heading
          fontSize="7xl"
          fontWeight="800"
          color="white"
          //   textShadow="5px 0px #4dcca3"
        >
          Gita P. Sujatmiko
        </Heading>
        <Text fontFamily="dmmono" color="#4dcca3">
          Computer Science Student at the University of Indonesia
        </Text>
      </VStack>
    </Box>
  );
};

export default Hero;
