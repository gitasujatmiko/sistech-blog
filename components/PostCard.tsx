import { Box, HStack, Text } from "@chakra-ui/layout";
import { Input, Link } from "@chakra-ui/react";
import { EditIcon } from "@chakra-ui/icons";
import { LikeButton } from "@lyket/react";
import { useForm } from "react-hook-form";
import axios from "axios";

export type PostCardProps = {
  postId: string;
  title: string;
  content: string;
  like: number;
  onClick: VoidFunction;
};

const PostCard = ({
  postId,
  title,
  content,
  like,
  onClick,
  ...props
}: PostCardProps) => {
  const { register, handleSubmit } = useForm();

  const likeButtonHandler = async (like: any) => {
    const response = await axios.put(
      "https://sistech-api.vercel.app/blog/",
      like,
      {
        headers: {
          Accept: "application/json",
          Authorization: `Bearer ${process.env.MY_BEARER_TOKEN}`,
        },
      }
    );
  };

  return (
    <Box
      padding="30px"
      width="500px"
      shadow="lg"
      borderRadius="md"
      marginBottom="30px"
      bg="gray.200"
      {...props}
    >
      <Box>
        <Text fontWeight="bold" fontSize="24px">
          {title}
        </Text>
        <Text>{content}</Text>
        <HStack float="right" mt="2">
          <Link
            // href={{
            //   pathname: `/blog/${postId}/edit`,
            //   query: {
            //     title,
            //     content,
            //   },
            // }}
            href={`/blog/${postId}/edit`}
          >
            <EditIcon />
          </Link>
          {/* <Input
            type="hidden"
            {...register("post", {
              id: postId,
              title: title,
              content: content,
              like: like + 1,
            })}
          /> */}
          <LikeButton id="like-post" />
        </HStack>
      </Box>
    </Box>
  );
};

export default PostCard;
