import { VStack } from "@chakra-ui/layout";
import Navbar from "../components/Navbar";

const Layout = ({ children, ...props }) => {
  return (
    <VStack spacing="0" bg="#232931" {...props}>
      <Navbar />
      {children}
    </VStack>
  );
};

export default Layout;
