import { Box, HStack, UnorderedList, ListItem, Text } from "@chakra-ui/layout";

const Experience = () => {
  return (
    <Box px="5" py="200" w="100%" color="white">
      <HStack spacing="100px" display="flex" justifyContent="center">
        <Text fontSize="4xl" fontWeight="700">
          Experiences
        </Text>
        <UnorderedList spacing="3">
          <ListItem>
            <Text fontSize="lg" fontWeight="600">
              Teaching Assistant for Computer Networks
            </Text>
            <Text fontSize="md" fontWeight="400" fontFamily="dmmono">
              Faculty of Computer Science, University of Indonesia (07/2021 -
              01/2022)
            </Text>
          </ListItem>
          <ListItem>
            <Text fontSize="lg" fontWeight="600">
              Web Developer & Designer Volunteer
            </Text>
            <Text fontSize="md" fontWeight="400" fontFamily="dmmono">
              IT Force Bureau of FUKI Fasilkom University of Indonesia (02/2020
              - 01/2022)
            </Text>
          </ListItem>
          <ListItem>
            <Text fontSize="lg" fontWeight="600">
              Teaching Assistant for Programming Foundations 1
            </Text>
            <Text fontSize="md" fontWeight="400" fontFamily="dmmono">
              Faculty of Computer Science, University of Indonesia (09/2020 -
              01/2021)
            </Text>
          </ListItem>
        </UnorderedList>
      </HStack>
    </Box>
  );
};

export default Experience;
