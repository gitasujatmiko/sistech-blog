import { Box, Flex, Spacer, Text } from "@chakra-ui/layout";
import NavLink from "./NavLink";

const Navbar = () => {
  return (
    <>
      <Box as="nav" color="white" p="5" px="12" w="100%" bg="#232931">
        <Flex>
          <Box>
            <Text fontFamily="inter" fontWeight="700">
              Gita&lsquo;s Website
            </Text>
          </Box>
          <Spacer />
          <Box>
            <NavLink mr="5" url="/">
              Home
            </NavLink>
            <NavLink url="/blog">Blog</NavLink>
          </Box>
        </Flex>
      </Box>
    </>
  );
};

export default Navbar;
