import { Link } from "@chakra-ui/layout";

const NavLink = ({ children, url, ...props }) => (
  <Link
    p="3"
    fontWeight="medium"
    fontFamily="dmmono"
    borderBottom="3.5px solid #4dcca3"
    _hover={{
      color: "#232931",
      textDecoration: "none",
      backgroundColor: "#4dcca3",
      padding: "3",
    }}
    href={url}
    {...props}
  >
    {children}
  </Link>
);

export default NavLink;
