/** @type {import('next').NextConfig} */
const nextConfig = {
  reactStrictMode: true,
  swcMinify: true,
  env: {
    MY_BEARER_TOKEN: process.env.MY_BEARER_TOKEN,
    LYKET_PUBLIC_API_KEY: process.env.LYKET_PUBLIC_API_KEY,
  },
};

module.exports = nextConfig;
