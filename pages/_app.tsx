import { ChakraProvider } from "@chakra-ui/react";
import { Provider } from "@lyket/react";
import theme from "../theme";

function MyApp({ Component, pageProps }) {
  return (
    <Provider apiKey={process.env.LYKET_PUBLIC_API_KEY}>
      <ChakraProvider theme={theme}>
        <Component {...pageProps} />
      </ChakraProvider>
    </Provider>
  );
}

export default MyApp;
