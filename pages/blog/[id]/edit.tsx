import {
  FormControl,
  FormLabel,
  Input,
  Box,
  Button,
  Text,
  Textarea,
  useToast,
} from "@chakra-ui/react";
import { useForm } from "react-hook-form";
import { useRouter } from "next/router";
import axios from "axios";
import Layout from "../../../components/Layout";

const BlogEdit = () => {
  const { register, handleSubmit } = useForm();
  const router = useRouter();
  const title = router.query.title;
  const content = router.query.content;

  const formSubmitHandler = async (data: any) => {
    const response = await axios.put(
      "https://sistech-api.vercel.app/blog/",
      data,
      {
        headers: {
          Accept: "application/json",
          Authorization: `Bearer ${process.env.MY_BEARER_TOKEN}`,
        },
      }
    );
  };

  return (
    <Layout backgroundColor="gray.300">
      <Box height="100vh" width="50%" padding="10">
        <Text fontSize="5xl" fontWeight="bold">
          Edit Blog
        </Text>
        <form onSubmit={handleSubmit(formSubmitHandler)}>
          <FormControl isRequired>
            <FormLabel>Title</FormLabel>
            <Input
              //   initialValue={title}
              focusBorderColor="#232931"
              borderColor="gray.800"
              _hover={{
                borderColor: "gray.800",
              }}
              placeholder="Blog Title"
              {...register("title")}
            />
            <br />
            <br />
            <FormLabel>Content</FormLabel>
            <Textarea
              //   initialValue={content}
              focusBorderColor="#232931"
              borderColor="gray.800"
              _hover={{
                borderColor: "gray.800",
              }}
              placeholder="Lorem ipsum dolor sit amet"
              {...register("content")}
            />
          </FormControl>

          <Button
            bg="#232931"
            color="white"
            mt="7"
            type="submit"
            _hover={{ backgroundColor: "#4dcca3", color: "#232931" }}
          >
            Submit
          </Button>
        </form>
      </Box>
    </Layout>
  );
};

export default BlogEdit;
