import { GetServerSideProps, GetStaticProps } from "next";
import { Box, Center, Heading, Link, HStack, VStack } from "@chakra-ui/react";
import { Button } from "@chakra-ui/button";
import { AddIcon } from "@chakra-ui/icons";
import Layout from "../../components/Layout";
import { useRouter } from "next/router";
import axios from "axios";
import { Post } from "../../types";
import PostCard from "../../components/PostCard";

interface PostJsonResponse {
  data: Post[];
}

export const getStaticProps: GetStaticProps = async () => {
  const response = await axios.get("https://sistech-api.vercel.app/blog/", {
    headers: {
      Accept: "application/json",
      Authorization: `Bearer ${process.env.MY_BEARER_TOKEN}`,
    },
  });
  const data: Post[] = response.data;

  return {
    props: {
      data,
    },
  };
};

const Blog = ({ data }: PostJsonResponse) => {
  const router = useRouter();
  const toPostView = (id: string) => router.push(`/blog/${id}`);
  const posts = data.map((post) => (
    <PostCard
      key={post.id}
      postId={post.id}
      title={post.title}
      content={post.content}
      like={post.like}
      onClick={() => toPostView(post.id)}
    />
  ));

  return (
    <Layout backgroundColor="gray.300" h="100%">
      <Box height="100vh" padding="10">
        <HStack display="flex" justifyContent="space-between" mb="7">
          <Heading>My Blog</Heading>
          <Button as="a" href="/blog/create" leftIcon={<AddIcon />}>
            Create a post
          </Button>
        </HStack>
        <Center>
          <VStack>{posts}</VStack>
        </Center>
      </Box>
    </Layout>
  );
};

export default Blog;
