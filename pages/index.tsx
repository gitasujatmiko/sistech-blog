import Layout from "../components/Layout";
import Hero from "../components/Hero";
import Achievement from "../components/Achievement";
import Experience from "../components/Experience";

const Index = () => {
  return (
    <Layout>
      <Hero />
      <Achievement />
      <Experience />
    </Layout>
  );
};

export default Index;
