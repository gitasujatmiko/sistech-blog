interface Post {
  id: string;
  title: string;
  content: string;
  like: number;
}

export { Post };
